package cn.alubi.lpresearch_library.lpsensorlib;

import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DataLogger {

    private static final String TAG = "DataLogger";
    File logFile;
    FileOutputStream logFileStream;
    OutputStreamWriter logFileWriter;
    boolean isLoggingStarted = false;

    String statusMesg = "Logging Stopped";
    String outputFilename ="";

    public DataLogger(){

    }

    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    public boolean startLogging() {
        boolean status = false;
        if (isExternalStorageWritable() == true) {
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
                String currentDateandTime = sdf.format(new Date());

                File logFileDir = new File(Environment.getExternalStorageDirectory()+ "/LpLogger");
                logFileDir.mkdirs();

                logFile = new File(logFileDir, "DataLog" + currentDateandTime + ".csv");
                logFile.createNewFile();

                logFileStream = new FileOutputStream(logFile);
                logFileWriter = new OutputStreamWriter(logFileStream);
                logFileWriter.append(
                        "SensorId,"+
                        "TimeStamp (s)," +
                        "FrameNumber,"+
                        " AccX (g), AccY (g), AccZ (g), " +
                        "GyroX (deg/s), GyroY (deg/s), GyroZ (deg/s), " +
                        "MagX (uT), MagY (uT), MagZ (uT), " +
                        //"AngVelX ,AngVelY ,AngVelZ ,"+
                        "EulerX(deg) , EulerY(deg) , EulerZ(deg) , "+
                        "QuatW, QuatX, QuatY, QuatZ, " +
                        "LinAccX(g) , LinAccY(g) , LinAccZ(g) ,"+
                        "Pressure(kPa)," +
                        "Altitude(m)," +
                        "Temperature(degC)," +
                        "BatteryLevel(%)+\n");

                isLoggingStarted = true;
                outputFilename = logFile.getAbsolutePath();
                statusMesg = "Logging to " +  outputFilename;
                status = true;
            } catch (Exception e) {
            	 Log.e(TAG, e.getMessage());
                statusMesg = e.getMessage();
            }
        } else {
            statusMesg = "Couldn't write to external storage. Please detach device from PC";
        }
        return status;
    }

    public boolean stopLogging() {
        boolean status = false;
        if (!isLoggingStarted) {
            statusMesg = "Logging not started";
            return status;
        }
        try {
            isLoggingStarted = false;

            synchronized(logFileWriter) {
                logFileWriter.close();
            }

            logFileStream.close();

            statusMesg = "Logging Stopped";
            status = true;
        } catch (Exception e) {
            statusMesg = e.getMessage();
        }
        return status;
    }

    public boolean isLogging(){
        return isLoggingStarted;

    }

    public boolean logLpmsData(LpmsBData d) {
        boolean status = false;
        if (isLoggingStarted) {
            try {
                synchronized(logFileWriter) {
                    logFileWriter.append(
                            d.imuId+","
                            + d.timestamp + ", "
                            +d.frameNumber+","
                            + d.acc[0] + ", " + d.acc[1] + ", " + d.acc[2] + ", "
                            + d.gyr[0] + ", " + d.gyr[1] + ", " + d.gyr[2] + ", "
                            + d.mag[0] + ", " + d.mag[1] + ", " + d.mag[2] + ", "
                           // + d.angVel[0] +", "+ d.angVel[1] + ", " +d.angVel[2] + ", "
                            + d.euler[0] +", "+ d.euler[1] +", "+ d.euler[2] +", "
                            + d.quat[0] + ", " + d.quat[1] + ", " + d.quat[2] + ", " + d.quat[3] + ", "
                            + d.linAcc[0] +", "+ d.linAcc[1] +", "+ d.linAcc[2] +", "
                            + d.pressure + ", "
                            + d.altitude + ", "
                            + d.temperature+","
                            + d.batteryLevel
                            + "\n");
                }
                status = true;
            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
                statusMesg = e.getMessage();
            }
        }
        return status;
    }

    public void setStatusMesg(String s) {
        statusMesg = s;
    }

    public String getStatusMesg(){
        return statusMesg;
    }

    public String getOutputFilename(){
        return outputFilename;
    }

    /////////////////////////
    //Flash Debug Data Log
    /////////////////////////

    boolean isDebugLoggingStarted = false;
    OutputStreamWriter debugLogFileWriter;
    int debugFrameNumber = 0;
    String debugOutputFilename;
    String debugStatusMesg;

    public boolean isDebugLogging(){
        return isDebugLoggingStarted;

    }

    public boolean startDebugLogging() {
        boolean status = false;
        if (isExternalStorageWritable() == true) {
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
                String currentDateandTime = sdf.format(new Date());

                File logFileDir = new File(Environment.getExternalStorageDirectory()+ "/LpLogger");
                logFileDir.mkdirs();

                File DebuglogFile = new File(logFileDir, "FlashDataLog" + currentDateandTime + ".csv");
                DebuglogFile.createNewFile();

                debugLogFileWriter = new OutputStreamWriter(new FileOutputStream(DebuglogFile));
                debugFrameNumber = 0;
                debugLogFileWriter.append(
                        "SensorId,"+
                        "TimeStamp (s)," +
                        "FrameNumber,"+
                        " AccX (g), AccY (g), AccZ (g), " +
                        "GyroX (deg/s), GyroY (deg/s), GyroZ (deg/s), " +
                        "MagX (uT), MagY (uT), MagZ (uT), " +
                        "QuatW, QuatX, QuatY, QuatZ, " +
                        "Pressure(kPa)," +
                        "Temperature(degC)\n");

                isDebugLoggingStarted = true;
                debugOutputFilename = DebuglogFile.getAbsolutePath();
                debugStatusMesg = "Logging to " +  debugOutputFilename;
                status = true;
            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
                debugStatusMesg = e.getMessage();
            }
        } else {
            debugStatusMesg = "Couldn't write to external storage. Please detach device from PC";
        }
        return status;
    }

    public boolean stopDebugLogging() {
        boolean status = false;
        if (!isDebugLoggingStarted) {
            debugStatusMesg = "Logging not started";
            return status;
        }
        try {
            isDebugLoggingStarted = false;

            synchronized(debugLogFileWriter) {
                debugLogFileWriter.close();
            }

            debugStatusMesg = "Logging Stopped";
            status = true;
        } catch (Exception e) {
            debugStatusMesg = e.getMessage();
        }
        return status;
    }

    public boolean logDebugLpmsData(LpmsBData d) {
        boolean status = false;
        if (isDebugLoggingStarted) {
            try {
                synchronized(debugLogFileWriter) {
                    debugLogFileWriter.append(
                            d.imuId+","
                            + d.timestamp + ", "
                            +d.frameNumber+","
                            + d.acc[0] + ", " + d.acc[1] + ", " + d.acc[2] + ", "
                            + d.gyr[0] + ", " + d.gyr[1] + ", " + d.gyr[2] + ", "
                            + d.mag[0] + ", " + d.mag[1] + ", " + d.mag[2] + ", "
                            + d.quat[0] + ", " + d.quat[1] + ", " + d.quat[2] + ", " + d.quat[3] + ", "
                            + d.pressure + ", "
                            + d.temperature+ "\n");
                }
                status = true;
            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
                statusMesg = e.getMessage();
            }
        }
        return status;
    }


}
